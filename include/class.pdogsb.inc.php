<?php
/** 
 * Classe d'accès aux données. 
 
 * Utilise les services de la classe PDO
 * pour l'application GSB
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO 
 * $monPdoGsb qui contiendra l'unique instance de la classe
 
 * @package default
 * @author Cheri Bibi
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoGsb{   		
      	private static $serveur='mysql:host=localhost';
      	private static $bdd='dbname=gsb';   		
      	private static $user='root' ;    		
      	private static $mdp='' ;	
		private static $monPdo;
		private static $monPdoGsb=null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */				
	public function __construct(){
    	PdoGsb::$monPdo = new PDO(PdoGsb::$serveur.';'.PdoGsb::$bdd, PdoGsb::$user, PdoGsb::$mdp); 
		PdoGsb::$monPdo->query("SET CHARACTER SET utf8");
	}
	public function _destruct(){
		PdoGsb::$monPdo = null;
	}
        /**
         * Fonction statique qui crée l'unique instance de la classe
         * Appel : $instancePdoGsb = PdoGsb::getPdoGsb();
         * @return l'unique objet de la classe PdoGsb
         */
	public  static function getPdoGsb(){
		if(PdoGsb::$monPdoGsb==null){
			PdoGsb::$monPdoGsb= new PdoGsb();
		}
		return PdoGsb::$monPdoGsb;  
	}
        
        
        /**
         * Retourne les informations d'un visiteur
         * @param $login 
         * @param $mdp
         * @return l'id, le nom et le prénom sous la forme d'un tableau associatif 
        */
                public function getInfosVisiteur($login, $mdp){
		$req = "select visiteur.idVisiteur as id, visiteur.nom as nom, visiteur.prenom as prenom from visiteur 
		where visiteur.login='$login' and visiteur.mdp='$mdp'";
		$rs = PdoGsb::$monPdo->query($req);
		$ligne = $rs->fetch(PDO::FETCH_ASSOC);
		return $ligne;
	}
        
        
        /**
         * Retourne les informations d'un comptable
         * @param type $login
         * @param type $mdp
         * @return l'id, le nom et le prenom du comptable 
         */
	public function getInfosComptable($login, $mdp){
		$req = "select comptable.idComptable as id, comptable.nom as nom, comptable.prenom as prenom from comptable 
		where comptable.login='$login' and comptable.mdp='$mdp'";
		$rs = PdoGsb::$monPdo->query($req);
		$ligne = $rs->fetch(PDO::FETCH_ASSOC);
		return $ligne;
	}
        
        
        /**
         * Recherche l'id du visiteur selon le mois et l'année choisi
         * @param type $mois
         * @param type $annee
         * @return l'id du visiteur du mois et année choisi
         */
        public function getVisiteur($mois, $annee){
            $requete = "select fichefrais.visiteur as visiteur from fichefrais where "
                    . "fichefrais.mois = '$mois' and fichefrais.annee ='$annee' order by fichefrais.visiteur desc ";
            $resultat = PdoGsb::$monPdo->query($requete);
            $lesVisiteurs=array();
                $laLigne = $resultat->fetch();
                while($laLigne != null){
                        $visiteur = $laLigne['visiteur'];
                        $lesVisiteurs["$visiteur"]=array("visiteur"=>"$visiteur");
                        $laLigne = $resultat->fetch();
                }
                return $lesVisiteurs;
        }
        
        
        /** 
         * Recherche l'id du visiteur
         * @param type $mois
         * @param type $annee
         * @return le nom du visiteur selon le mois et l'année choisi
         */
        public function getNomVisiteursValiderFicheFrais($mois, $annee){           
            $requete = "select visiteur.nom as nom from visiteur join fichefrais on fichefrais.visiteur = visiteur.idVisiteur where fichefrais.mois = '$mois' and fichefrais.annee = '$annee'";   
            $resultat = PdoGsb::$monPdo->query($requete);
            $nomVisiteur = array();
		$laLigne = $resultat->fetch(PDO::FETCH_ASSOC);
		while($laLigne != null)	{
			$visiteur = $laLigne['nom'];
			$nomVisiteur["$visiteur"]=array(
                            "nom"=>"$visiteur",
                         );
			$laLigne = $resultat->fetch(); 		
		}
		return $nomVisiteur;
        }

        
        /**
         *Recherche l'id du visiteur
         * @param type $mois
         * @param type $annee
         * @return le prénom du visiteur selon le mois et l'année choisi
         */
        public function getPrenomVisiteursValiderFicheFrais($mois, $annee){           
            $requete = "select visiteur.prenom from visiteur join fichefrais on fichefrais.visiteur = visiteur.idVisiteur where fichefrais.mois = '$mois' and fichefrais.annee = '$annee'";   
            $resultat = PdoGsb::$monPdo->query($requete);
            $prenomVisiteur = array();
		$laLigne = $resultat->fetch();
		while($laLigne != null)	{
			$visiteur = $laLigne['prenom'];
			$prenomVisiteur["$visiteur"]=array(
                            "prenom"=>"$visiteur",
                         );
			$laLigne = $resultat->fetch(); 		
		}
		return $prenomVisiteur;
        }
        
        
        /**
         * Recherche les mois
         * @return les fiche frais qui ont pour état CL
         */
        public function getLesMoisValiderFicheFrais(){
            $requete = "select fichefrais.mois from fichefrais where fichefrais.etat = 'CL' order by fichefrais.mois desc ";
            $resultat = PdoGsb::$monPdo->query($requete);
            $lesMois =array();
		$laLigne = $resultat->fetch();
		while($laLigne != null)	{
			$mois = $laLigne['mois'];
			$lesMois["$mois"]=array(
                            "mois"=>"$mois",
                         );
			$laLigne = $resultat->fetch(); 		
		}
		return $lesMois;
        }
        
        
        /**
         * Recherche les années
         * @return les fiche frais qui ont pour état CL
         */
        public function getLesAnneesValiderFicheFrais(){
            $requete = "select fichefrais.annee from fichefrais where fichefrais.etat = 'CL' order by fichefrais.mois desc ";
            $resultat = PdoGsb::$monPdo->query($requete);
            $lesAnnees = array();
		$laLigne = $resultat->fetch();
		while($laLigne != null)	{
			$annee = $laLigne['annee'];
			$lesAnnees["$annee"]=array(
                            "annee"=>"$annee",
                         );
			$laLigne = $resultat->fetch(); 		
		}
		return $lesAnnees;
        }        
              
        
        /**
         * Recherche l'id du visiteur
         * @param type $nom
         * @param type $prenom
         * @return l'id du visiteur
         */
        public function getIdVisiteur($nom, $prenom){   
            $requete = "select idVisiteur from visiteur where visiteur.nom = '$nom' and visiteur.prenom ='$prenom'";
            $resultat = PdoGsb::$monPdo->query($requete);
            $idVisiteur = array();
		$laLigne = $resultat->fetch();
		while($laLigne != null)	{
			$id = $laLigne['idVisiteur'];
			$idVisiteur["$id"]=array(
                            "idVisiteur"=>"$id",
                         );
			$laLigne = $resultat->fetch(); 		
		}
		return $idVisiteur;
        }
        

        
        /**
         * Recherche la fiche de frais
         * @param type $mois
         * @param type $annee
         * @return la fiche de frais selon le mois et l'année choisi
         */
        public function getFicheFrais($mois, $annee){
            $requete = "select idFicheFrais from fichefrais where fichefrais.mois = '$mois' "
                    . "and fichefrais.annee = '$annee'";
            $resultat = PdoGsb::$monPdo->query($requete);
            $ligne = $resultat->fetch();
            return $ligne;
        }

        
        /**
         * Retourne sous forme d'un tableau associatif toutes les lignes de frais hors forfait
         * concernées par les deux arguments

         * La boucle foreach ne peut être utilisée ici car on procède
         * à une modification de la structure itérée - transformation du champ date-
         * @param type $mois
         * @param type $annee
         * @return le tableau des frais forfait
         */
        public function getLesFraisForfait($mois, $annee){
	    $requete = PdoGsb::$monPdo->prepare("select fraisforfait.libelle as libelle, fraisforfait.montant as montant,"
                . " lignefraisforfait.quantite as quantite, lignefraisforfait.idLigneFraisForfait as idLigneFraisForfait from fraisforfait "
                . "join lignefraisforfait on fraisforfait.idFraisForfait = lignefraisforfait.fraisForfait "
                . "join fichefrais on lignefraisforfait.ficheFrais = fichefrais.idFicheFrais where "
                . "fichefrais.mois ='$mois' and fichefrais.annee = '$annee'");              
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
            return $lesLignes; 
	}
        
        
        
        /**
         * Retourne sous forme d'un tableau associatif toutes les lignes de frais hors forfait
         * concernées par les deux arguments

         * La boucle foreach ne peut être utilisée ici car on procède
         * à une modification de la structure itérée - transformation du champ date-
         * @param type $mois
         * @param type $annee
         * @return le tableau des frais hors forfait
         */
	public function getLesFraisHorsForfait($mois, $annee){
	    $requete = PdoGsb::$monPdo->prepare("select date, libelle, montant, "
                . "idLigneFraisHorsForfait from lignefraishorsforfait JOIN fichefrais "
                . "ON lignefraishorsforfait.ficheFrais = fichefrais.idFicheFrais "
                . "where estRefuse = false and fichefrais.mois = '$mois' and fichefrais.annee ='$annee'");
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
            return $lesLignes; 
	}

        
        /**
         * Retourne les informations d'une fiche de frais d'un visiteur pour un mois donné

         * @param $idVisiteur 
         * @param $mois sous la forme aaaamm
         * @return un tableau avec des champs de jointure entre une fiche de frais et la ligne d'état 
        */	
	public function getLesInfosFicheFrais($mois, $annee){
            $requete = "select etat.libelle as libelle, fichefrais.montantValide as montantValide, fichefrais.nbJustificatifs as nbJustificatifs, 
                fichefrais.dateModification as dateModification from etat
                join fichefrais on fichefrais.etat = etat.idEtat join visiteur on fichefrais.visiteur = visiteur.idVisiteur
                where fichefrais.mois ='$mois' and fichefrais.annee='$annee'";
            $resultat = PdoGsb::$monPdo->query($requete);
            $ligne = $resultat->fetch();
            return $ligne;
	}
        
        
        /**
         * Calcul le montant selon la quantité et le montant des lignes frais forfait  
         * @param type $mois
         * @param type $annee
         * @return le montant total des frais forfait
         */
	public function getPrixTotalFraisForfait($mois, $annee){
	        $requete = PdoGsb::$monPdo->prepare("select sum(quantite*montant) as prixTotal from fraisforfait "
                        . "join lignefraisforfait on fraisforfait.idFraisForfait = lignefraisforfait.fraisForfait "
                        . "join fichefrais on lignefraisforfait.ficheFrais = fichefrais.idFicheFrais where "
                        . "fichefrais.mois ='$mois' and fichefrais.annee = '$annee'");               
                $requete->execute(array());
		$lesLignes = $requete->fetchAll();
		return $lesLignes; 
	}  
        
        /**
         * Calcul le montant selon la quantité et le montant des lignes frais forfait  
         * @param type $mois
         * @param type $annee
         * @return le montant total des frais hors forfait
         */
	public function getPrixTotalFraisHorsForfait($mois, $annee){
	    $requete = PdoGsb::$monPdo->prepare("select sum(montant) as montant"
                . " from lignefraishorsforfait JOIN fichefrais "
                . "ON lignefraishorsforfait.ficheFrais = fichefrais.idFicheFrais "
                . "where fichefrais.mois = '$mois' and fichefrais.annee ='$annee' and estRefuse = false");
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
            return $lesLignes; 
	}
      
        
        /**
         * Met à jour le montant valide dans le suivi du paiement des fiches de frais
         * @param type $lemontantValide
         * @param type $mois
         * @param type $annee
         * @return le montant valide à jour
         */
        public function setMontantValide($lemontantValide, $mois, $annee){
                $requete = PdoGsb::$monPdo->prepare("update fichefrais set fichefrais.montantValide = $lemontantValide
			where fichefrais.mois ='$mois' and fichefrais.annee = '$annee'");               
                $requete->execute(array());
		$lesLignes = $requete->fetchAll();
		return $lesLignes; 
        }
        
        
        /**
         * Retourne le nombre de justificatif d'un visiteur pour un mois donné
         *@param $idVisiteur 
         * @param $mois sous la forme aaaamm
         * @return le nombre entier de justificatifs 
        */
	public function getNbjustificatifs($idVisiteur, $mois){
            $req = "select fichefrais.nbjustificatifs as nb from fichefrais where fichefrais.visiteur ='$idVisiteur' and fichefrais.mois = '$mois'";
            $res = PdoGsb::$monPdo->query($req);
            $laLigne = $res->fetch();
            return $laLigne['nb'];
	}
        
        
        /**
         * Retourne sous forme d'un tableau associatif toutes les lignes de frais au forfait
         * concernées par les deux arguments

         * @param $idVisiteur 
         * @param $mois sous la forme aaaamm
         * @return l'id, le libelle et la quantité sous la forme d'un tableau associatif 
        */
        public function getQuantiteFraisForfait($ficheDeFrais){
            $requete = PdoGsb::$monPdo->prepare("select lignefraisforfait.quantite as quantite from"
                . "lignefraisforfait where lignefraisforfait.ficheFrais = '$ficheDeFrais'");              
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
            return $lesLignes; 
        }
        

        
/**
 * Retourne tous les id de la table FraisForfait
 
 * @return un tableau associatif 
*/
	public function getLesIdFrais(){
		// Code à ajouter
		//return $lesLignes;
	}
/**
 * Met à jour la table ligneFraisForfait
 
 * Met à jour la table ligneFraisForfait pour un visiteur et
 * un mois donné en enregistrant les nouveaux montants
 
 * @param $idVisiteur 
 * @param $mois sous la forme aaaamm
 * @param $lesFrais tableau associatif de clé idFrais et de valeur la quantité pour ce frais
 * @return un tableau associatif 
*/
	public function majFraisForfait($ficheDeFrais, $quantite, $idLigneFraisForfait){
            $requete = PdoGsb::$monPdo->prepare("update lignefraisforfait "
                    . "set quantite = '$quantite' where fichefrais = '$ficheDeFrais' "
                    . "and idLigneFraisForfait = '$idLigneFraisForfait'");               
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
	    return $lesLignes; 
	}
     
        
        /**
         * met à jour le nombre de justificatifs de la table ficheFrais
         * pour le mois et le visiteur concerné

         * @param $idVisiteur 
         * @param $mois sous la forme aaaamm
        */
	public function majNbJustificatifs($idVisiteur, $mois, $nbJustificatifs){
		// Code à ajouter	
	}
        
        
        /**
         * Teste si un visiteur possède une fiche de frais pour le mois passé en argument

         * @param $idVisiteur 
         * @param $mois sous la forme aaaamm
         * @return vrai ou faux 
        */	
	public function estPremierFraisMois($idVisiteur,$mois)
	{
		// Code à ajouter
	}
        
        
        /**
         * Retourne le dernier mois en cours d'un visiteur

         * @param $idVisiteur 
         * @return le mois sous la forme aaaamm
        */	
	public function dernierMoisSaisi($idVisiteur){
		// Code à ajouter
	}
	
        
        /**
         * Crée une nouvelle fiche de frais et les lignes de frais au forfait pour un visiteur et un mois donnés

         * récupère le dernier mois en cours de traitement, met à 'CL' son champs idEtat, crée une nouvelle fiche de frais
         * avec un idEtat à 'CR' et crée les lignes de frais forfait de quantités nulles 
         * @param $idVisiteur 
         * @param $mois sous la forme aaaamm
        */
	public function creeNouvellesLignesFrais($idVisiteur,$mois){
		// Code à ajouter
	}
        
        
        /**
         * Crée un nouveau frais hors forfait pour un visiteur un mois donné
         * à partir des informations fournies en paramètre

         * @param $idVisiteur 
         * @param $mois sous la forme aaaamm
         * @param $libelle : le libelle du frais
         * @param $date : la date du frais au format français jj//mm/aaaa
         * @param $montant : le montant
        */
	public function creeNouveauFraisHorsForfait($idVisiteur,$mois,$libelle,$date,$montant){
            
	}
        
        
        /**
         * Refuse un frais hors forfait et disparait dans l'interface, mais pas dans la base de donnée
         * @param type $idLigneFraisHorsForfait
         * @return le refus d'un frais hors forfait
         */
        public function refuserFraisHorsForfait($idLigneFraisHorsForfait){
            $requete = PdoGsb::$monPdo->prepare("update lignefraishorsforfait "
                    . "set estRefuse = true where idLigneFraisHorsForfait = '$idLigneFraisHorsForfait'");               
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
	    return $lesLignes; 
        }
        
        
        public function reporterFraisHorsForfait($laDate, $idLigneFraisHorsForfait){
            $requete = PdoGsb::$monPdo->prepare("update lignefraishorsforfait "
                    . "set date = '$laDate' where idLigneFraisHorsForfait = '$idLigneFraisHorsForfait'");               
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
	    return $lesLignes;             
        }
        
        
        /**
         * Valide la fiche de frais et sera alors affiché dans le suivi du paiement des fiches de frais
         * @param type $ficheDeFrais
         * @return l'id de la fiche de frais en true
         */
        public function validerFicheFrais($ficheDeFrais){
            $requete = PdoGsb::$monPdo->prepare("update fichefrais set estValide = true, etat = 'CR' where idFicheFrais = '$ficheDeFrais'");
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
	    return $lesLignes; 
        }

    
        // PARTIE SUIVI  
        /**
         * Recherche les fiches de frais validées
         * @return le nom, le prénom, le montant valide, la date de modification et l'état du fiche de frais validé
         */
        public function getLesFiches(){
	    $requete = PdoGsb::$monPdo->prepare("select idFicheFrais, idVisiteur, nom, prenom, mois, annee, montantValide, "
                . "dateModification, etat from Visiteur JOIN fichefrais ON Visiteur.idVisiteur "
                . "= fichefrais.visiteur where estValide = true and etat = 'CR' or etat = 'VA' order by etat");
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
            return $lesLignes; 
	}
        
        /**
         * @param type $idFiche
         * @return l'id de la fiche frais passé en paramètre
         */
        public function getFicheFraisVA($idFiche){
            $requete = "select idFicheFrais from fichefrais where fichefrais.idFicheFrais = '$idFiche'";
            $resultat = PdoGsb::$monPdo->query($requete);
            $ligne = $resultat->fetch();
            return $ligne;
        }
        
         /**
         * Modifie l'état et la date de modification d'une fiche de frais
         * Modifie le champ idEtat
         * @param $idVisiteur 
         * @param $mois sous la forme aaaamm
         */
        public function majEtatFicheFrais($idFicheFrais){
            $requete = PdoGsb::$monPdo->prepare("update fichefrais set etat = 'VA' where idFicheFrais = '$idFicheFrais'");
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
	    return $lesLignes;             
        }
        
        /**
         * Modifie l'état en Remboursé
         * @param type $idFicheFrais
         * @return type
         */
        public function lesFichesRembourser($idFicheFrais){
            $requete = PdoGsb::$monPdo->prepare("update fichefrais set etat = 'RB' where idFicheFrais = '$idFicheFrais'");               
            $requete->execute(array());
            $lesLignes = $requete->fetchAll();
            return $lesLignes; 
        }
        
        
        public function getLesFichesPdf(){
            $requetes = "select nom, prenom, adresse, mois, dateModification, libelle, montant, montantValide, ville "
                    . "from gsb.Visiteur join gsb.fichefrais on visiteur.idVisiteur = fichefrais.visiteur "
                    . "join gsb.lignefraishorsforfait on lignefraishorsforfait.ficheFrais = fichefrais.idFicheFrais";
            $resultat = PdoGsb::$monPdo->query($requetes);
            $affichePdf = array();

           // $res = PdoGsb::$monPdo->prepare($req);
           //$lesLignes = $requetes->fetchAll();
            while($affichagePdf = $resultat->fetch(PDO::FETCH_OBJ)){
                $affichePdf[] = $affichagePdf;
            }
            return ($affichePdf); 
        }       
}
?>