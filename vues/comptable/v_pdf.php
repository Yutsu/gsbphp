<?php
   
    function creerPdf($montrerPdf){   
        // permet d'inclure la bibliothèque fpdf
        require('fpdf181/fpdf.php');
        // instancie un objet de type FPDF qui permet de créer le PDF
        $pdf = new FPDF();
        // ajoute une page
        $pdf->AddPage();
        // définit la police courante
        $pdf->SetFont('Arial','B',16);
        // affiche une image
        //$pdf->Cell(10,10,"Galaxy-Swiss Bourdin");
        $pdf->Image('images/logo.jpg', 63, 25, 90, 60);
        // affiche du texte        
        $pdf->Ln(10);
        $pdf->Cell(10,150,'Visiteur : ' . $montrerPdf->nom . ' '.$montrerPdf->prenom);
        $pdf->Ln(10);
        $pdf->Cell(10,150,'Mois : ' . $montrerPdf->mois);
        $pdf->Ln(85);
        
        // premier tableau affichage
        $pdf->Cell(60,10, 'Frais forfaitaires', 1,0, 'C');
        $pdf->Cell(30,10, 'Quantite', 1,0, 'C');
        $pdf->Cell(50,10, 'Montant unitaire', 1,0, 'C');
        $pdf->Cell(20,10, 'Total', 1,0, 'C');
       /*
        while($data = $montrerPdf->fetch(PDO::FETCH_OBJ)){
            $pdf->cell(20,10, $data->idFraisForfait, 1,0, 'C');
            $pdf->cell(20,10, $data->quantite, 1,0, 'C');
            $pdf->cell(20,10, $data->montant, 1,0, 'C');
        }*/
                
        $pdf->Ln(30);
        $pdf->Cell(10,10,"Autres frais");
        
        // deuxième tableau affichage
        $pdf->Ln(30);
        $pdf->Cell(40,10, 'Date', 1,0, 'C');
        $pdf->Cell(70,10, 'Libelle', 1,0, 'C');
        $pdf->Cell(50,10, 'Montant ', 1,0, 'C');
        
        $proprietesTableau = array(
	'BRD_COLOR' => array(0,255,0),
	'BRD_SIZE' => '0.3',
	'TB_ALIGN' => 'L',
	'L_MARGIN' => 15,
	);
        $contenuTableau = array(
	"mon texte", "la colonne 2, ligne 1", "le contenu de ma troisième cellule",
	"Salut 1", 1, 2,
	"champ 2", 3, 4
	);	
        
        $pdf->drawTableau($pdf, $proprietesTableau, $contenuTableau);
        
        /*
        while($data = $montrerPdf->fetch(PDO::FETCH_OBJ)){
            $pdf->cell(20,10, $data->date, 1,0, 'C');
            $pdf->cell(20,10, $data->libelle, 1,0, 'C');
            $pdf->cell(20,10, $data->montant, 1,0, 'C');
        }*/

        
        // dernière partie
        $pdf->Ln(20);
        $pdf->Cell(10,10,"Total : $montrerPdf->total euros");
        $pdf->Ln(10);
        $pdf->Cell(10,10,"Montant Refuse : ");
        $pdf->Ln(10);
        $pdf->Cell(10,10,"Montant Valide : $montrerPdf->montantValide euros");
        $pdf->Ln(10);
        $pdf->Cell(10,10,"Fait a : $montrerPdf->ville");
        $pdf->Ln(10);
        $pdf->Cell(10,10,"Vu par l'agent comptable ");

        //$pdf->Cell(3,150,'Adresse : ' . $montrerPdf->adresse);
        $pdf->Ln(10);
        //$pdf->Cell(3,150,'Date Modification : ' . $montrerPdf->dateModification);
        $pdf->Ln(10);
        //$pdf->Cell(3,150,'Libelle : ' . $montrerPdf->libelle);
        $pdf->Ln(10);
        //$pdf->Cell(3,150,'Montant : ' . $montrerPdf->montant . " euros");
        //$pdf->Cell(10,170,'Montant : ' . $fiche->montant);
        // Enfin, le document est terminé et envoyé au navigateur grâce à Output().
        ob_get_clean();
        $pdf->Output();
    }
