
<div id="contenu">
    <!-- CECILE : 10 Octobre -->
      <h2>Valider fiches de frais</h2>
      <h3>Choisissez le visiteur à sélectionner </h3>
      <form action="index.php?uc=afficherFrais&action=voirEtatFrais" method="post">
      <div class="corpsForm">
         
      <p>
        <label for="ficheVisiteur" accesskey="n">Visiteur : </label>
        <select id="ficheVisiteur" name="ficheVisiteur" type='hidden' value="<?php echo $_SESSION['ficheVisiteur']?>">
            <?php       
                foreach ($lesNomVisiteurs as $unNomVisiteur){
                    foreach ($lesPrenomVisiteurs as $unPrenomVisiteur ){     
                        
                        $nomVisiteur = $unNomVisiteur['nom'];
                        $prenomVisiteur = $unPrenomVisiteur['prenom'];
                        
                        if($nomVisiteur == $nomVisiteurASelectionner && $prenomVisiteur == $prenomVisiteurASelectionner){
                        ?>
                            <option selected value="<?php echo $prenomVisiteur." ".$nomVisiteur ?>"><?php echo  $prenomVisiteur." ".$nomVisiteur ?> </option>
                        <?php 
                        }
                            
                        else{ ?> 
                            <option value="<?php echo $prenomVisiteur." ".$nomVisiteur ?>"><?php echo $prenomVisiteur." ".$nomVisiteur ?> </option>
                        <?php 
                        }
                    }
		}
            ?>    
        </select>
      </p>     
      
      </div>
      <div class="piedForm">
      <p>
        <input id="ok" type="submit" value="Valider" size="20" />
      </p> 
      </div>
        
      </form>
</div>