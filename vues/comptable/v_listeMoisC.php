
<div id="contenu">
    <!-- CECILE : 10 Octobre -->
      <h2>Valider fiches de frais</h2>
      <h3>Choisissez le mois/année à sélectionner </h3>
      <form action="index.php?uc=afficherFrais&action=selectionnerVisiteur" method="post">
      <div class="corpsForm">
         
      <p>
        <label for="moisAnnee" accesskey="n">Mois : </label>
        <select id="moisAnnee" name="moisAnnee">
            <?php       
                foreach ($lesMois as $unMois){
                    foreach($lesAnnees as $uneAnnee){
                        $mois = $unMois['mois'];
                        $annee = $uneAnnee['annee'];

                        if($mois == $moisASelectionner && $annee == $anneeASelectionner){
                        ?>
                            <option selected value="<?php echo $mois."/".$annee?>"> <?php echo $mois."/".$annee ?> </option>
                        <?php 
			}
			
                        else{ ?> 
                            <option value="<?php echo $mois."/".$annee?>"> <?php echo $mois."/".$annee ?> </option>
                        <?php 
			} 
                    }			       		
		}           
            ?>    
        </select>
      </p>
      </div>
      <div class="piedForm">
      <p>
        <input id="ok" type="submit" value="Valider" size="20" />
      </p> 
      </div>
      </form>