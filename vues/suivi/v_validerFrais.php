 <div id="contenu">

<h3>Fiche de frais numéro <?php echo $ficheFrais ?> du mois <?php echo $leMoisVisiteur."/".$lanneeVisiteur ?> : 
    </h3>
    <div class="encadre">
    <p>
        Etat : <?php echo $libelle ?> depuis le <?php echo $dateModif?> <br> <!Montant validé : <?php echo $montantValide?> €->                     
    </p>
    <br>
  	<table class="listeLegere">
            <caption>Eléments forfaitisés :</caption>
        <tr>        
        <th></th>
        <?php
            foreach ( $lesFraisForfait as $unFraisForfait ){
                $libelle = $unFraisForfait['libelle'];
            ?>	
                <th> <?php echo $libelle?></th>
            <?php
            }          
            ?>   
	</tr>                
        <tr>
         <th>Montant initial </th>
         <?php  
            foreach ( $lesFraisForfait as $unFraisForfait ){
                $montant = $unFraisForfait['montant'];
            ?>	
                <td> <?php echo $montant?> € </td>
            <?php
            }
            ?>
	</tr>       
        <tr>
        <th>Quantité </th>
        <?php
          foreach ($lesFraisForfait as $unFraisForfait) {
		$quantite = $unFraisForfait['quantite'];
            ?>
                <td class="qteForfait"><?php echo $quantite?> </td>
            <?php
            }
            ?>
	</tr>
    </table>   
    <table class="listeLegere">
         <tr>
        <?php
            foreach ($lesPrixTotalFraisForfait as $unPrixTotalFraisForfait ){
                $prixTotal = $unPrixTotalFraisForfait['prixTotal'];
            ?>	
                <th> Montant des frais forfait </th>
                <td> <?php echo $prixTotal?> euros</td>
            <?php
            }
            ?>
	</tr>
    </table>
   
    <br><br>
    <table class="listeLegere">
  	   <caption>Descriptif des éléments hors forfait : <?php echo $nbJustificatifs ?> justificatifs reçus
       </caption>
             <tr>
                <th class="date">Date</th>
                <th class="libelle">Libellé</th>
                <th class='montant'>Montant</th>                  
             </tr>
             
        <?php      
           
          foreach ( $lesFraisHorsForfait as $unFraisHorsForfait ){
                    $date = $unFraisHorsForfait['date'];
                    $libelle = $unFraisHorsForfait['libelle'];
                    $montant = $unFraisHorsForfait['montant'];
		?>
             <tr>
                <td><?php echo $date ?></td>
                <td><?php echo $libelle ?></td>
                <td><?php echo $montant ?> € </td>
             </tr>
        <?php 
          }
        ?>
    </table>
    <table class="listeLegere">
         <tr>
        <?php
            foreach ($lesPrixTotalFraisHorsForfait as $unPrixTotalFraisHorsForfait ){
                $montant = $unPrixTotalFraisHorsForfait['montant'];
            ?>	
                <th> Montant des frais hors forfait </th>
                <td> <?php echo $montant?> euros</td>
            <?php
            }
            ?>
	</tr>
    </table>
    <br><br>
    <table class="listeLegere">
         <tr>
        <?php
            foreach ($lesPrixTotalFraisForfait as $unPrixTotalFraisForfait ){
                foreach($lesPrixTotalFraisHorsForfait as $unPrixTotalFraisHorsForfait){
                    $prixTotal = $unPrixTotalFraisForfait['prixTotal'];
                    $montant = $unPrixTotalFraisHorsForfait['montant'];
                ?>
                <th> PRIX TOTAL </th>
                <td> <?php echo $prixTotal+$montant?> euros </td>

                <?php
                }
            }
            $_SESSION['total'] = $prixTotal+$montant
            ?>
    </table>   
    <td>
        <form action="index.php?uc=payerFrais&action=rembourserFicheFrais" method="post">  
            <input type=hidden name=ficheFrais value=ficheFrais id="ficheFrais">
            <input type=hidden name=visiteur value=visiteur id="visiteur">
            <input type="submit" value="Rembourser la fiche de frais">
        </form>
    </td>        
  

    </div>