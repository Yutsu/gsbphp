<h3>Fiche de frais numéro <?php echo $ficheFrais ?> du mois <?php echo $leMoisVisiteur."/".$lanneeVisiteur ?> : 
    </h3>
    <div class="encadre">
    <p>
        Etat : <?php echo $libelle ?> depuis le <?php echo $dateModif?> <br> <!Montant validé : <?php echo $montantValide?> €->                     
    </p>
    <br>
  	<table class="listeLegere">
            <caption>Eléments forfaitisés :</caption>
        <tr>        
        <th></th>
        <?php
            foreach ( $lesFraisForfait as $unFraisForfait ){
                $libelle = $unFraisForfait['libelle'];
            ?>	
                <th> <?php echo $libelle?></th>
            <?php
            }          
            ?>   
	</tr>                
        <tr>
         <th>Montant initial </th>
         <?php  
            foreach ( $lesFraisForfait as $unFraisForfait ){
                $montant = $unFraisForfait['montant'];
            ?>	
                <td> <?php echo $montant?> € </td>
            <?php
            }
            ?>
	</tr>       
        <tr>
        <th>Quantité </th>
        <?php
          foreach ($lesFraisForfait as $unFraisForfait) {
		$quantite = $unFraisForfait['quantite'];
            ?>
                <td class="qteForfait"><?php echo $quantite?> </td>
            <?php
            }
            ?>
	</tr>
    </table>   
    <table class="listeLegere">
         <tr>
        <?php
            foreach ($lesPrixTotalFraisForfait as $unPrixTotalFraisForfait ){
                $prixTotal = $unPrixTotalFraisForfait['prixTotal'];
            ?>	
                <th> Montant des frais forfait </th>
                <td> <?php echo $prixTotal?> euros</td>
            <?php
            }
            ?>
	</tr>
    </table>
    <td>
        <form action="index.php?uc=afficherFrais&action=modifierFrais" method="post">
            <input type=hidden name=moisVisiteur value=moisVisiteur id="moisVisiteur">
            <input type=hidden name=anneeVisiteur value=anneeVisiteur id="anneeVisiteur">
            <input type=hidden name=ficheFrais value=ficheFrais id="ficheFrais">
            <input type="submit" value="Modifier les quantités">
        </form>
    </td>  
    <br><br>
    <table class="listeLegere">
  	   <caption>Descriptif des éléments hors forfait : <?php echo $nbJustificatifs ?> justificatifs reçus
       </caption>
             <tr>
                 <th class="id">Id</th>
                <th class="date">Date</th>
                <th class="libelle">Libellé</th>
                <th class='montant'>Montant</th>   
                <th class='refuser'>Refuser</th>                
                <th class='reporter'>Reporter</th>                
             </tr>
             
        <?php      
           
          foreach ( $lesFraisHorsForfait as $unFraisHorsForfait ){
                    $id = $unFraisHorsForfait['idLigneFraisHorsForfait'];
                    $date = $unFraisHorsForfait['date'];
                    $libelle = $unFraisHorsForfait['libelle'];
                    $montant = $unFraisHorsForfait['montant'];
		?>
             <tr>
                <td><?php echo $id ?></td>
                <td><?php echo $date ?></td>
                <td><?php echo $libelle ?></td>
                <td><?php echo $montant ?> € </td>
                <td>
                    <form action="index.php?uc=afficherFrais&action=refuserFrais" method="post">
                        <input type=hidden name=idLigneFraisHorsForfait value="<?php echo $id?>" id="ligneFraisHorsForfait">
                        <input type="submit" value="Refuser">
                    </form>
                </td>       
                <td>
                    <form action="index.php?uc=afficherFrais&action=reporterFrais" method="post">
                        <input type=hidden name=ladate value="<?php echo $date?>" id="date">
                        <input type=hidden name=idLigneFraisHorsForfait value="<?php echo $id?>" id="ligneFraisHorsForfait">
                        <input type="submit" value="Reporter">
                    </form>
                </td> 
             </tr>
        <?php 
          }
        ?>
    </table>
    <table class="listeLegere">
         <tr>
        <?php
            foreach ($lesPrixTotalFraisHorsForfait as $unPrixTotalFraisHorsForfait ){
                $montant = $unPrixTotalFraisHorsForfait['montant'];
            ?>	
                <th> Montant des frais hors forfait </th>
                <td> <?php echo $montant?> euros</td>
            <?php
            }
            ?>
	</tr>
    </table>
    <br><br>
    <table class="listeLegere">
         <tr>
        <?php
            foreach ($lesPrixTotalFraisForfait as $unPrixTotalFraisForfait ){
                foreach($lesPrixTotalFraisHorsForfait as $unPrixTotalFraisHorsForfait){
                    $prixTotal = $unPrixTotalFraisForfait['prixTotal'];
                    $montant = $unPrixTotalFraisHorsForfait['montant'];
                ?>
                <th> PRIX TOTAL </th>
                <td> <?php echo $prixTotal+$montant?> euros </td>

                <?php
                }
            }
            $_SESSION['total'] = $prixTotal+$montant
            ?>
    </table>   
    <td>
        <form action="index.php?uc=afficherFrais&action=validerFrais" method="post">  
            <input type=hidden name=ficheFrais value=ficheFrais id="ficheFrais">
            <input type=hidden name=visiteur value=visiteur id="visiteur">
            <input type=hidden name=moisVisiteur value=moisVisiteur id="moisVisiteur">
            <input type="submit" value="Valider la fiche de frais">
        </form>
    </td>        
  </div>
</div>