<?php

class incTest extends PHPUnit_Framework_TestCase {

    protected $monPdotest;

    protected function setUp(){
        require_once("../class.pdogsb.inc.php") ;
        $this->monPdotest = PdoGsb::getPdoGsb();
        PdoGsb::getPdo()->exec("SET AUTOCOMMIT OFF;");
        PdoGsb::getPdo()->beginTransaction();  
    }
    
    protected function tearDown(){
       PdoGsb::getPdo()->rollBack();
       PdoGsb::getPdo()->exec("SET AUTOCOMMIT ON;");
    }
    
    public function testGetInfosVisiteur() {
        $visiteur = array(
            'idVisiteur' => 'a131',
            'nom' => "Villechalane",
            'prenom' => "Louis"
        );   
        $return = $this->monPdotest->getInfosVisiteur('lvillachane', 'jux7g') ;
        $this->assertEquals($visiteur,$return);    
    }
    
    public function testGetInfosComptable(){
        $visiteur = array(
            'idcomptable' => '1',
            'nom' => "Rollback" ,
            'prenom' => "Lou"
        );   
        $return = $this->monPdotest->getInfosVisiteur('roll', 'root') ;
        $this->assertEquals($visiteur,$return);           
    }
    
    public function testValiderFicheFrais(){
        $valider = array(
            'estValide' => true
        );
        
        $return = $this->monPdotest->validerFicheFrais(4) ;
        $this->assertEquals($valider, $return);      
                
    }
    
}
