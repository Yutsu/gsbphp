<?php
include("vues/comptable/v_sommaireC.php");
$idVisiteur = $_SESSION['idVisiteur'];
$mois = getMois(date("d/m/Y"));
$numAnnee =substr( $mois,0,4);
$numMois =substr( $mois,4,2);
$action = $_REQUEST['action'];
switch($action){
	case 'saisirFrais':{
		if($pdo->estPremierFraisMois($idVisiteur,$mois)){
			$pdo->creeNouvellesLignesFrais($idVisiteur,$mois);
		}
		break;
	}
	case 'validerMajFraisForfait':{
		$lesFrais = $_REQUEST['lesFrais'];
		if(lesQteFraisValides($lesFrais)){
	  	 	$pdo->majFraisForfait($idVisiteur,$mois,$lesFrais);
		}
		else{
			ajouterErreur("Les valeurs des frais doivent être numériques");
			include("vues/v_erreurs.php");
		}
	  break;
	}
	case 'validerCreationFrais':{
		$dateFrais = $_REQUEST['dateFrais'];
		$libelle = $_REQUEST['libelle'];
		$montant = $_REQUEST['montant'];
		valideInfosFrais($dateFrais,$libelle,$montant);
		if (nbErreurs() != 0 ){
			include("vues/v_erreurs.php");
		}
		else{
			$pdo->creeNouveauFraisHorsForfait($idVisiteur,$mois,$libelle,$dateFrais,$montant);
		}
		break;
	}
       
                        
        case 'reporterFrais':{
            $nomVisiteur = $_SESSION['nomVisiteur'];
            $leMoisVisiteur = $_SESSION['moisVisiteur']; 
            $lanneeVisiteur =  $_SESSION['anneeVisiteur'];
            $_SESSION['leMoisVisiteur'] = $leMois;
            //$_SESSION['lstVisiteurC'] = $idVisiteur;
            $idLigneFraisHorsForfait = $_REQUEST['idFrais'];
            $pdo->reporterFraisHorsForfait($idLigneFraisHorsForfait);
            break;
        }

        
        case 'reporterFrais':{
            $idLigneFraisHorsForfait = $_REQUEST['idFrais'];
            $pdo->reporterFraisHorsForfait($idLigneFraisHorsForfait);
            break;
        }
        
	case 'supprimerFrais':{
		$idFrais = $_REQUEST['idFrais'];
                $pdo->supprimerFraisHorsForfait($idFrais);
		break;
	}
}
$lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($idVisiteur,$mois);
$lesFraisForfait= $pdo->getLesFraisForfait($idVisiteur,$mois);
include("vues/v_listeFraisForfait.php");
include("vues/v_listeFraisHorsForfait.php");

?>