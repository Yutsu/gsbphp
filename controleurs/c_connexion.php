<?php
if(!isset($_REQUEST['action'])){
	$_REQUEST['action'] = 'demandeConnexion';
}
$action = $_REQUEST['action'];
switch($action){
	case 'demandeConnexion':{
		include("vues/login/v_connexion.php");
		break;
	}
	case 'valideConnexion':{
		$login = $_REQUEST['login'];
		$mdp = $_REQUEST['mdp'];
                
                
		$visiteur = $pdo->getInfosVisiteur($login,SHA1($mdp));
                $comptable = $pdo->getInfosComptable($login, SHA1($mdp));
                
                // PARTIE VISITEUR
                if($visiteur){
                    if(!is_array($visiteur)){
                            ajouterErreur("Login ou mot de passe incorrect");
                            include("vues/login/v_erreurs.php");
                            include("vues/login/v_connexion.php");
                    }
                    else{
                            $id = $visiteur['id'];
                            $nom =  $visiteur['nom'];
                            $prenom = $visiteur['prenom'];
                            $a=connecter($id,$nom,$prenom);
                            include("vues/visiteur/v_sommaire.php");
                            include("vues/visiteur/v_accueil.php");
                    }
                }
                                  
                // PARTIE COMPTABLE
                else{
                    if(!is_array($comptable)){
                            ajouterErreur("Login ou mot de passe incorrect");
                            include("vues/login/v_erreurs.php");
                            include("vues/login/v_connexion.php");
                    }
                    else{
                            $id = $comptable['id'];
                            $nom =  $comptable['nom'];
                            $prenom = $comptable['prenom'];
                            connecter($id,$nom,$prenom);
                            include("vues/comptable/v_sommaireC.php");
                            include("vues/comptable/v_accueilC.php");

                    }
                }
		break;
	}
	default :{
		include("vues/login/v_connexion.php");
		break;
	}
}
?>