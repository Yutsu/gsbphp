<?php

include("vues/comptable/v_sommaireC.php");
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
switch($action){
    case 'voirSuivi':{
        $lesFiches = $pdo->getLesFiches();
        include("vues/suivi/v_suivreFiche.php");
        break;
    }
    
    case 'validerFicheFrais':{
        
        $leMoisVisiteur = $_SESSION['moisVisiteur'];
        $lanneeVisiteur =  $_SESSION['anneeVisiteur'];
        $idFiche = $_POST['leFrais'];

        $lesFicheFrais=$pdo->getFicheFraisVA($idFiche); //Récupère les fiches qui sont en VA
        $pdo->majEtatFicheFrais($idFiche); // Etat en CR ou VA (car si pas encore remboursé)
        $_SESSION['leFrais'] = $lesFicheFrais; //Prêt à l'envoi pour la page suivante (l.49)grâce à la Session 

        $lesPrixTotalFraisForfait = $pdo->getPrixTotalFraisForfait($leMoisVisiteur, $lanneeVisiteur);
        $lesPrixTotalFraisHorsForfait = $pdo->getPrixTotalFraisHorsForfait($leMoisVisiteur, $lanneeVisiteur);  

        $lemontantValide = $_SESSION['total'];
        $pdo->setMontantValide($lemontantValide, $leMoisVisiteur, $lanneeVisiteur);

        $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($leMoisVisiteur, $lanneeVisiteur);
        $lesFraisForfait= $pdo->getLesFraisForfait($leMoisVisiteur, $lanneeVisiteur);
        $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($leMoisVisiteur, $lanneeVisiteur); 

        $ficheFrais = $lesFicheFrais['idFicheFrais'];
        $libelle = $lesInfosFicheFrais['libelle'];
        $montantValide = $lesInfosFicheFrais['montantValide'];
        $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];

        $dateModif =  $lesInfosFicheFrais['dateModification'];
        $dateModif =  dateAnglaisVersFrancais($dateModif);
        
        include("vues/suivi/v_validerFrais.php");
        break;
    }
    
    case 'rembourserFicheFrais':{
          $ficheFrais = $_SESSION['leFrais']; //Réception de la Session (l.24)
          $ficheDeFrais = $ficheFrais['idFicheFrais'];
          $pdo->lesFichesRembourser($ficheDeFrais);         
          include("vues/suivi/v_rembourserFrais.php");
        break;
    }
    
    case 'pdf':{
        $fichePdf= $pdo->getLesFichesPdf();
        $afficherPdf = $_REQUEST['fichePdf'];
        $montrerPdf = $fichePdf[$afficherPdf];
        include ("vues/comptable/v_pdf.php");
        $res = creerPdf($montrerPdf);
        break;
    }
    
}

?>
