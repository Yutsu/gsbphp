<?php
include("vues/comptable/v_sommaireC.php");
$action = $_REQUEST['action'];
$idVisiteur = $_SESSION['idVisiteur'];
//select idVisiteur ... where nom et prenom
switch($action){
	case 'selectionnerMois':{
            $lesMois=$pdo->getLesMoisValiderFicheFrais();
            $lesAnnees=$pdo->getLesAnneesValiderFicheFrais();

            $lesCles = array_keys($lesMois);
            $lesCles1 = array_keys($lesAnnees);
    
            $moisASelectionner = $lesCles[0];
            $anneeASelectionner = $lesCles1[0];
    
            include("vues/comptable/v_listeMoisC.php");
            break;
	}
        
        case 'selectionnerVisiteur':{
            $leMois = $_REQUEST['moisAnnee'];
            
            $mois = substr($leMois,0,2);
            $annee = substr($leMois, 3);
            
            
            $_SESSION['moisVisiteur'] = $mois;
            $moisVisiteur = $_SESSION['moisVisiteur'];   
           
            $_SESSION['anneeVisiteur'] = $annee;
            $anneeVisiteur = $_SESSION['anneeVisiteur'];
                
            $lesNomVisiteurs=$pdo->getNomVisiteursValiderFicheFrais($moisVisiteur, $anneeVisiteur);                               
            $lesPrenomVisiteurs=$pdo->getPrenomVisiteursValiderFicheFrais($moisVisiteur, $anneeVisiteur);                               

            $lesCles = array_keys($lesNomVisiteurs);
            $lesCles2 = array_keys($lesPrenomVisiteurs);

            $nomVisiteurASelectionner = $lesCles[0];
            $prenomVisiteurASelectionner = $lesCles2[0];

            include("vues/comptable/v_listeVisiteurC.php");
            break;           
        }

        case 'voirEtatFrais':{      
            /*
            $ficheVisiteur = $_REQUEST['ficheVisiteur'];
           
            $espaceUn = strpos($ficheVisiteur, " ");
            $espaceDeux = strpos($ficheVisiteur, " ",$espaceUn+1);
            
            $nomV = substr($ficheVisiteur, $espaceUn+1);
            $prenomV = substr($ficheVisiteur,0,$espaceUn);            
            
            $idDuVisiteur = $pdo->getIdVisiteur($nomV, $prenomV);
            var_dump($idDuVisiteur);*/
            
            $leMoisVisiteur = $_SESSION['moisVisiteur']; //l.27
            $lanneeVisiteur =  $_SESSION['anneeVisiteur'];
          
           
            $lesMois=$pdo->getLesMoisValiderFicheFrais();
            $lesAnnees=$pdo->getLesAnneesValiderFicheFrais();
                   
            $lesFicheFrais=$pdo->getFicheFrais($leMoisVisiteur, $lanneeVisiteur);
            $_SESSION['ficheFrais'] = $lesFicheFrais;
            
            $lesPrixTotalFraisForfait = $pdo->getPrixTotalFraisForfait($leMoisVisiteur, $lanneeVisiteur);
            $lesPrixTotalFraisHorsForfait = $pdo->getPrixTotalFraisHorsForfait($leMoisVisiteur, $lanneeVisiteur);  
            
            $lemontantValide = $_SESSION['total'];
            $pdo->setMontantValide($lemontantValide, $leMoisVisiteur, $lanneeVisiteur);
                      
            $lesFraisHorsForfait = $pdo->getLesFraisHorsForfait($leMoisVisiteur, $lanneeVisiteur);
            $lesFraisForfait= $pdo->getLesFraisForfait($leMoisVisiteur, $lanneeVisiteur);
            $lesInfosFicheFrais = $pdo->getLesInfosFicheFrais($leMoisVisiteur, $lanneeVisiteur); 

            $ficheFrais = $lesFicheFrais['idFicheFrais'];
            $libelle = $lesInfosFicheFrais['libelle'];
            $montantValide = $lesInfosFicheFrais['montantValide'];
            $nbJustificatifs = $lesInfosFicheFrais['nbJustificatifs'];
            
            $dateModif =  $lesInfosFicheFrais['dateModification'];
            $dateModif =  dateAnglaisVersFrancais($dateModif);
            
            include("vues/comptable/v_listeMoisC.php");           
            include("vues/v_etatFrais.php");
            break;
	}
        
        case 'modifierFrais':{
            $ficheFrais = $_SESSION['ficheFrais'];
            $ficheDeFrais = $ficheFrais['idFicheFrais'];
            $leMoisVisiteur = $_SESSION['moisVisiteur']; 
            $lanneeVisiteur =  $_SESSION['anneeVisiteur'];
            
            $lesLibelles = $pdo->getLesFraisForfait($leMoisVisiteur, $lanneeVisiteur);
            include("vues/v_modifierFrais.php");
            break;
        }
        
        case 'modifierFraisValider':{                        
            $ficheFrais = $_SESSION['ficheFrais'];
            $ficheDeFrais = $ficheFrais['idFicheFrais'];
            //var_dump($ficheDeFrais);
        
            $quantite = $_POST['laQuantite'];
            //var_dump($quantite);

            $idLigneFraisForfait = $_POST['laLigne'];
            //var_dump($idLigneFraisForfait);
          
            $pdo->majFraisForfait($ficheDeFrais, $quantite, $idLigneFraisForfait);
            include("vues/v_modifierFraisValider.php"); 
            break;
        }

        case 'refuserFrais':{
            /*
            $idLigneFraisHorsForfait = $_SESSION['idFraisHorsForfait']; 
            var_dump($idLigneFraisHorsForfait);
            $pdo->refuserFraisHorsForfait($idLigneFraisHorsForfait);*/
            
            /*
            $idLigneFraisHorsForfait = $_REQUEST['idLigneFraisHorsForfait']; 
            //var_dump($idLigneFraisHorsForfait);*/
            
            $ligneHorsForfait = $_POST['idLigneFraisHorsForfait'];
            $pdo->refuserFraisHorsForfait($ligneHorsForfait);
            include("vues/v_refuserFrais.php");    
            break;
        }
        
        case 'reporterFrais':{
            $ligneHorsForfait = $_POST['idLigneFraisHorsForfait'];
            $date = $_POST['ladate'];
            $laDate = date('Y-m-d', strtotime($date.' +1 month'));
            $pdo->reporterFraisHorsForfait($laDate, $ligneHorsForfait);
            include("vues/v_reporterFrais.php");    
            break;
        }
       
        			
        case 'validerFrais':{
            $ficheFrais = $_SESSION['ficheFrais'];
            $ficheDeFrais = $ficheFrais['idFicheFrais'];
            $pdo->validerFicheFrais($ficheDeFrais);
            include("vues/v_validerFrais.php"); 
            break;
        }

        
}
